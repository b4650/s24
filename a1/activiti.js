//  1) Find users with letter 's' in their first name or 'd' in their last name.

db.users.find(
	{
		$or : [{firstName : {$regex : "s", $options : "i"}}, {lastName : {$regex : "d", $options : "i"}}]
	},
	{
		firstName : 1,
		lastName : 1,
		_id : 0
	}

);

// 2) Find users who are from the HR department 

db.users.find({
	$and : [ {department : "HR"}, {age : {$gte : 70}} ]
});

// 3) Find users with the letter 'e' in their first name 

db.users.find({
	$and : [ {firstName : { $regex : "e", $options : "i"}}, {age : {$lte : 30}} ]
});